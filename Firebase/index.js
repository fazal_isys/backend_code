const firebaseAdmin = require("firebase-admin");

const serviceAccount = require("./config.json");

const firebaseConfig = {
  credential: firebaseAdmin.credential.cert(serviceAccount),
  databaseURL: "your database url",
};
firebaseAdmin.initializeApp(firebaseConfig);

const firestoreDatabase = firebaseAdmin.firestore();
firestoreDatabase.settings({ timestampsInSnapshots: true });

const realtimeDatabase = firebaseAdmin.database();

module.exports = {
  firestoreDatabase,
  realtimeDatabase,
};
