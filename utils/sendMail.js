// Sending Email using mailgun
const mailgun_domain = "mg.walletly.ai";
const mailgun_api_key = "cd6c193c791e56485c9bda50f6d47619-73ae490d-ef19eae8";
var mailgun = require("mailgun-js")({
  apiKey: mailgun_api_key,
  domain: mailgun_domain,
});
const path = require("path");

module.exports = async function (from, to, subject, html) {
  try {
    const data = { from, to, subject, html };

    const result = await mailgun.messages().send(data);
    return {
      success: true,
      message: result,
    };
  } catch (error) {
    return {
      success: false,
      message: "Failed to sent.",
      error: error,
    };
  }
};
