const multer = require("multer");
const multerS3 = require("multer-s3");
const AWS = require("aws-sdk");
const path = require("path");
AWS.config.region = "us-west-2";

AWS.config.update({
  accessKeyId: "AKIAI6Q3AAW3X62WSL3Q",
  secretAccessKey: "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",
});

const s3 = new AWS.S3();

const upload = multer({
  storage: multerS3({
    s3: s3,
    bucket: "isystematic/Docs",
    acl: "public-read",
    contentType: multerS3.AUTO_CONTENT_TYPE,
    metadata: function (req, file, cb) {
      // console.log(req);
      cb(null, { fieldName: file.fieldname });
    },
    key: function (req, file, cb) {
      let date = new Date().getTime();

      cb(null, `doc/${date + "_" + file.originalname}`);
    },
  }),
  fileFilter: function (req, file, callback) {
    var ext = path.extname(file.originalname);

    if (ext !== ".doc" && ext !== ".docx" && ext !== ".pdf") {
      return callback(new Error("Only Doc and PDF are allowed"));
    }
    callback(null, true);
  },
  limits: {
    fileSize: 20097152,
  },
});

module.exports = upload;
