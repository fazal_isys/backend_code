const methods = require("./Methods");

module.exports = [
  {
    url: "/api/v1/verifyAuth",
    method: "post",
    execute: methods.verifyAuth,
  },
  {
    url: "/api/v1/setData",
    method: "post",
    execute: methods.postFormData,
  },
  {
    url: "/api/v1/updateForm/:id",
    method: "post",
    execute: methods.updateForm,
  },

  {
    url: "/api/v1/uploadDoc",
    method: "post",
    execute: methods.UploadDoc,
  },
  {
    url: "/api/v1/getData",
    method: "get",
    execute: methods.getData,
  },
  {
    url: "/api/v1/postTimelineData",
    method: "post",
    execute: methods.postTimelineData,
  },
  {
    url: "/api/v1/getTimelineData/:id",
    method: "get",
    execute: methods.getTimelineDataById,
  },
  {
    url: "/api/v1/postTaskData",
    method: "post",
    execute: methods.postTaskData,
  },
  {
    url: "/api/v1/getTaskData/:id",
    method: "get",
    execute: methods.getTaskDataById,
  },
  {
    url: "/api/v1/updateTask/:id",
    method: "post",
    execute: methods.updateTaskById,
  },
  {
    url: "/api/v1/postDocumentData",
    method: "post",
    execute: methods.postDocData,
  },
  {
    url: "/api/v1/getDocumentData/:id",
    method: "get",
    execute: methods.getDocByID,
  },
  {
    url: "/api/v1/updateDocument/:id",
    method: "post",
    execute: methods.updateDocByID,
  },
];
