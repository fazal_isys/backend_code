const firebase = require("firebase-admin");
const database = require("../../../Firebase/index").firestoreDatabase;
const formData = database.collection("FormData");
const sendMail = require("../sendNotificationMail");
module.exports = async (request) => {
  console.log("here");

  let { body } = request;

  const newRef = formData.doc();
  const newBody = {
    ...body,
    created_at: firebase.firestore.FieldValue.serverTimestamp(),
    status: "pending",
  };
  console.log(newBody);

  await newRef.set(newBody);
  // sendMail(body);
  return {
    success: true,
    message: "New Record Added",
  };
};
