const database = require("../../../Firebase/index").firestoreDatabase;
const formData = database.collection("FormData");

module.exports = async (request) => {
  const queryresult = await formData.get();

  const Data = [];
  if (queryresult.empty) {
    return {
      success: false,
      message: "No Data found",
    };
  } else {
    queryresult.forEach((records) => {
      const _json = {
        _id: records.id,
        ...records.data(),
      };

      Data.push(_json);
    });
    return {
      success: true,
      message: "Records Found",
      data: Data,
    };
  }
};
