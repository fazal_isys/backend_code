const upload = require("../../utils/s3");

const singleUpload = upload.single("doc");

module.exports = async (req) => {
  return new Promise((Resolve) => {
    singleUpload(req, null, async function (err, some) {
      if (err) {
        console.log(err);
        Resolve({
          success: true,
          message: "Doc Upload Error",
          error: err.message,
        });
      }

      const file = req.file;
      const doc = {
        fileName: file.originalname,
        location: file.key,
        url: file.location,
      };

      Resolve({
        success: true,
        message: "Doc uploaded",
        data: doc,
      });
    });
  });
};
