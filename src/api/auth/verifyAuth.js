const getUserByemail = require("../user/getUserByEmail");
const encryptPassword = require("./encryptPassword");

module.exports = async (request) => {
  const { email, password } = request.body;

  const result = await getUserByemail(email);
  if (result["success"]) {
    const userData = result.data;
    const oldPassword = await encryptPassword(password);
    if (oldPassword == userData.password) {
      delete userData.password;

      return {
        success: true,
        message: "User Found",
        data: userData,
      };
    } else {
      return {
        success: false,
        message: "Password Invalid",
      };
    }
  } else {
    return result;
  }
};
