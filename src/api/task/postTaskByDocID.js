const firebase = require("firebase-admin");
const database = require("../../../Firebase/index").firestoreDatabase;
const taskCollection = database.collection("tasks");

module.exports = async (request) => {
  console.log("in task post");

  let { body } = request;
  // console.log(body);

  const newRef = taskCollection.doc();
  const newBody = {
    ...body,
    delete: false,
    created_at: firebase.firestore.FieldValue.serverTimestamp(),
  };

  await newRef.set(newBody);
  return {
    success: true,
    message: "New Record Added",
  };
};
