const firebase = require("firebase-admin");
const database = require("../../../Firebase/index").firestoreDatabase;
const taskCollection = database.collection("tasks");

module.exports = async (request) => {
  const id = request.params.id;
  let { body } = request;
  console.log(id);
  console.log(body);

  const queryresult = await taskCollection.doc(id).update({
    ...body,
    updated_at: firebase.firestore.FieldValue.serverTimestamp(),
  });

  return {
    success: true,
    message: "Task Updated",
  };
};
