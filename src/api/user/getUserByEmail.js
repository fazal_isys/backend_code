const database = require("../../../Firebase/index").firestoreDatabase;
const userCollection = database.collection("user");
module.exports = async (email) => {
  const users = await userCollection.where("email", "==", email).get();
  if (users.empty) {
    return {
      success: false,
      message: "No user found",
    };
  } else {
    const userData = [];
    users.forEach((user) => {
      userData.push({ user_id: user.id, ...user.data() });
    });

    return {
      success: true,
      message: "User Found",
      data: userData[0],
    };
  }
};
