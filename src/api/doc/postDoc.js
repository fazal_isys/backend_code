const firebase = require("firebase-admin");
const database = require("../../../Firebase/index").firestoreDatabase;
const docCollection = database.collection("documents");

module.exports = async (request) => {
  console.log("in document post");

  let { body } = request;
  // console.log(body);

  const newRef = docCollection.doc();
  const newBody = {
    ...body,
    delete: false,
    created_at: firebase.firestore.FieldValue.serverTimestamp(),
  };

  await newRef.set(newBody);
  return {
    success: true,
    message: "New Record Added",
  };
};
