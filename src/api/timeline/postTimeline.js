const firebase = require("firebase-admin");
const database = require("../../../Firebase/index").firestoreDatabase;
const timelineCollection = database.collection("timeline");

module.exports = async (request) => {
  console.log("in Time line post");

  let { body } = request;
  // console.log(body);

  const newRef = timelineCollection.doc();
  const newBody = {
    ...body,
    delete: false,
    created_at: firebase.firestore.FieldValue.serverTimestamp(),
  };

  await newRef.set(newBody);
  return {
    success: true,
    message: "New Record Added",
  };
};
