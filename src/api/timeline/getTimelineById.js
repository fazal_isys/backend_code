const database = require("../../../Firebase/index").firestoreDatabase;
const timelineCollection = database.collection("timeline");

module.exports = async (request) => {
  const id = request.params.id;

  const queryresult = await timelineCollection.where("doc_id", "==", id).get();
  console.log("here");
  var counter = 0;
  const Data = [];
  if (queryresult.empty) {
    return {
      success: false,
      message: "No Data found",
    };
  } else {
    queryresult.forEach((records) => {
      if (records.data().inputPosition > counter)
        counter = records.data().inputPosition;
      const _json = {
        _id: records.id,
        date: new Date(records.data().created_at["_seconds"] * 1000),
        ...records.data(),
      };

      Data.push(_json);
    });

    return {
      success: true,
      message: "Records Found",
      inputPosition: counter + 1,
      data: Data,
    };
  }
};
