const sendMail = require("../../utils/sendMail");
const ejs = require("ejs");

module.exports = async function (UserData) {
  const { stackHolderEmail, stackHolderName, companyName } = UserData;
  const from = "noreply.walletly@gmail.com",
    subject = "New Request Received",
    data = {
      name: stackHolderName,
      email: stackHolderEmail,
      companyName,
    };

  let emailBody;

  ejs.renderFile("./views/notificationMail.ejs", { data: data }, function (
    err,
    str
  ) {
    if (str) {
      emailBody = str;
    } else if (err) {
      console.log(err);
    }
  });

  sendMail(from, "CEO@monsterhub.io", subject, emailBody);

  return {
    success: true,
  };
};
