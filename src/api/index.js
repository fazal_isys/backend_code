const postFormData = require("./form/postFormData");
const getData = require("./form/getData");
const updateForm = require("./form/updateForm");
const UploadDoc = require("./uploadDoc");
const verifyAuth = require("./auth/verifyAuth");
const postTimelineData = require("./timeline/postTimeline");
const getTimelineDataById = require("./timeline/getTimelineById");
const postTaskData = require("./task/postTaskByDocID");
const getTaskDataById = require("./task/getTaskByDocID");
const updateTaskById = require("./task/updateTackByID");
const postDocData = require("./doc/postDoc");
const getDocByID = require("./doc/getDocById");
const updateDocByID = require("./doc/updateDocById");

module.exports = {
  postFormData,
  getData,
  updateForm,
  UploadDoc,
  verifyAuth,
  postTimelineData,
  getTimelineDataById,
  postTaskData,
  getTaskDataById,
  updateTaskById,
  postDocData,
  getDocByID,
  updateDocByID,
};
